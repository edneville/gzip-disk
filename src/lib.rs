extern crate apache_rs;
extern crate chrono;
extern crate flate2;

use apache_rs::ffi::ap_send_fd;
use apache_rs::ffi::*;
use chrono::offset::Utc;
use chrono::DateTime;
use flate2::read::GzDecoder;
use io::BufReader;
use std::convert::TryInto;
use std::ffi::CStr;
use std::ffi::CString;
use std::fs::File;
use std::io;
use std::os::raw::c_char;
use std::os::raw::c_int;
use std::os::raw::c_void;
use std::ptr;

const VERSION: &str = env!("CARGO_PKG_VERSION");

struct Form {
    extension: String,
    found: bool,
    accept: bool,
    meta_data: Option<std::fs::Metadata>,
    file_name: Option<String>,
    encoding: String,
}

#[allow(unused_unsafe)]
#[no_mangle]
pub static mut gzip_disk_ng_module: module = module {
    version: MODULE_MAGIC_NUMBER_MAJOR as i32,
    minor_version: MODULE_MAGIC_NUMBER_MINOR as i32,
    module_index: -1,
    name: b"mod_gzip_disk_ng\x00" as *const u8 as *const c_char,
    dynamic_load_handle: 0 as *mut c_void,
    next: 0 as *mut module,
    magic: MODULE_MAGIC_COOKIE as u64,
    rewrite_args: None,
    create_dir_config: None,
    merge_dir_config: None,
    create_server_config: None,
    merge_server_config: None,
    flags: 0,
    cmds: 0 as *mut command_struct,
    register_hooks: Some(c_mod_gzip_disk_ng_hooks),
};

extern "C" fn c_mod_gzip_disk_ng_hooks(_: *mut apr_pool_t) {
    unsafe {
        ap_hook_handler(
            Some(c_mod_gzip_disk_ng_handler),
            std::ptr::null(),
            std::ptr::null(),
            APR_HOOK_MIDDLE.try_into().unwrap(),
        );
    };
}

#[allow(clippy::not_unsafe_ptr_arg_deref)]
pub fn rwrite(buffer: &Vec<u8>, r: *mut request_rec) {
    unsafe { ap_rwrite(buffer.as_ptr() as *mut c_void, buffer.len() as i32, r) };
}

#[allow(clippy::not_unsafe_ptr_arg_deref)]
pub fn set_header(key: &str, value: &str, r: *mut request_rec) {
    let name = CString::new(key).expect("CString::new failed");
    let value = CString::new(value).expect("CString::new failed");
    unsafe {
        apr_table_set((*r).headers_out, name.as_ptr(), value.as_ptr());
    }
}

#[allow(clippy::not_unsafe_ptr_arg_deref)]
pub fn decompress_writer(mut input: impl std::io::Read, r: *mut request_rec) -> io::Result<String> {
    let mut buffer = [0u8; 65535];
    loop {
        match input.read(&mut buffer) {
            Ok(size) => {
                if size == 0 {
                    break;
                }
                unsafe { ap_rwrite(buffer.as_ptr() as *mut c_void, size as i32, r) };
            }
            Err(x) => {
                return Err(x);
            }
        }
    }

    Ok("Done".to_string())
}

extern "C" fn c_mod_gzip_disk_ng_handler(r: *mut request_rec) -> c_int {
    // eprintln!("Loading");

    set_header("Mod-Gzip-Disk-Ng-Version", VERSION, r);
    let mut apr_file = ptr::null_mut();

    let file = unsafe {
        let file = CStr::from_ptr((*r).filename);
        let file = String::from(file.to_str().unwrap());
        file
    };

    let mut l = vec![
        Form {
            extension: "br".to_string(),
            found: false,
            accept: false,
            meta_data: None,
            file_name: None,
            encoding: "br".to_string(),
        },
        Form {
            extension: "zstd".to_string(),
            found: false,
            accept: false,
            meta_data: None,
            file_name: None,
            encoding: "zstd".to_string(),
        },
        Form {
            extension: "gz".to_string(),
            found: false,
            accept: false,
            meta_data: None,
            file_name: None,
            encoding: "gzip".to_string(),
        },
    ];

    unsafe {
        let accept_header = CString::new("Accept-Encoding").unwrap();
        let accept = apr_table_get((*r).headers_in, accept_header.as_ptr());
        if !accept.is_null() {
            // eprintln!("Accept is not null");
            let p = CStr::from_ptr(accept).to_str().unwrap();
            for f in l.iter_mut() {
                // eprintln!("Test Accept: {}", &f.extension);
                if p.contains('*') || p.contains(&f.encoding) {
                    // eprintln!("Accept: {}", &f.extension);
                    f.accept = true;
                }
            }
        }
    }

    let mut found_one = false;
    for f in l.iter_mut() {
        f.file_name = Some(format!("{}.{}", file, f.extension));

        let meta_data = std::fs::metadata(&f.file_name.clone().unwrap());
        if let Ok(m) = meta_data {
            // eprintln!("meta data for {} is ok accept is {}", &f.file_name.clone().unwrap(), f.accept);
            f.found = true;
            found_one = true;
            f.meta_data = Some(m);
        }
    }

    if !found_one {
        return DECLINED;
    }

    for f in l.iter() {
        if f.meta_data.is_some() && f.accept {
            // eprintln!("acceptable for {}", &f.file_name.clone().unwrap());
            let meta_data = f.meta_data.clone().unwrap();
            let modified = meta_data.modified().unwrap();

            let datetime: DateTime<Utc> = modified.into();
            let datetime = format!("{}", datetime.format("%a, %d %h %Y %H:%M:%S GMT"));

            set_header("Last-Modified", &datetime, r);

            set_header("Mod-Gzip-Disk-Ng", ":-)", r);
            unsafe {
                let mut file_info = apr_finfo_t {
                    pool: (*r).pool,
                    valid: 0,
                    protection: 0,
                    filetype: 0,
                    user: 0,
                    group: 0,
                    inode: 0,
                    device: 0,
                    nlink: 0,
                    size: 0,
                    csize: 0,
                    atime: 0,
                    mtime: 0,
                    ctime: 0,
                    fname: ptr::null(),
                    name: ptr::null(),
                    filehand: ptr::null_mut(),
                };
                let file_to_open =
                    CString::new(f.file_name.clone().unwrap()).expect("CString::new failed");
                apr_file_open(
                    &mut apr_file,
                    file_to_open.as_ptr(),
                    (APR_FOPEN_READ | APR_FOPEN_SENDFILE_ENABLED) as i32,
                    0,
                    (*r).pool,
                );
                apr_file_info_get(&mut file_info, APR_FINFO_NORM as i32, apr_file);
                set_header("Content-Encoding", &f.encoding, r);

                let len = file_info.size;
                let mut sent = 0;
                ap_send_fd(apr_file, r, 0, len.try_into().unwrap(), &mut sent);

                apr_file_close(apr_file);
                return OK as i32;
            }
        }
    }

    set_header("Mod-Gzip-Disk-Ng", ":-(", r);

    for f in l.iter() {
        if f.meta_data.is_none() {
            continue;
        }

        match f.extension.as_ref() {
            "gz" => {
                // eprintln!("yopes");
                let decoder = BufReader::new(File::open(f.file_name.as_ref().unwrap()).unwrap());
                let mut input = GzDecoder::new(decoder);

                return match decompress_writer(&mut input, r) {
                    Ok(_) => OK,
                    Err(_) => HTTP_INTERNAL_SERVER_ERROR,
                } as i32;
            }
            "br" => {
                // eprintln!("decompressing br");
                let mut input = brotli::Decompressor::new(
                    File::open(f.file_name.as_ref().unwrap()).unwrap(),
                    4096,
                );
                return match decompress_writer(&mut input, r) {
                    Ok(_) => OK,
                    Err(_) => HTTP_INTERNAL_SERVER_ERROR,
                } as i32;
            }
            "zstd" => {
                // eprintln!("decompressing zstd");

                let file = File::open(f.file_name.as_ref().unwrap()).unwrap();
                let decoder = zstd::Decoder::new(file).unwrap();

                let mut input = BufReader::new(decoder);
                return match decompress_writer(&mut input, r) {
                    Ok(_) => OK,
                    Err(_) => HTTP_INTERNAL_SERVER_ERROR,
                } as i32;
            }

            _ => {
                // eprintln!("nopes: {} ", &f.extension);
                return DECLINED;
            }
        }
    }

    DECLINED
}
